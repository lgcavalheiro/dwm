static const char norm_fg[] = "#f6e0cd";
static const char norm_bg[] = "#191410";
static const char norm_border[] = "#ac9c8f";

static const char sel_fg[] = "#f6e0cd";
static const char sel_bg[] = "#D27B84";
static const char sel_border[] = "#f6e0cd";

static const char urg_fg[] = "#f6e0cd";
static const char urg_bg[] = "#4757A0";
static const char urg_border[] = "#4757A0";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};

